import pygame
from pygame.locals import *
from Class_Grille_V2 import *
from effet import *
from afficheur import *


# Ouverture de la fenetre Pygame
fenetre = pygame.display.set_mode((1300,700))

#Definition des couleurs
blue = [0, 255, 255]

def accueil(delai):
    pygame.init()    
    
    # Titre de la fenetre
    pygame.display.set_caption("Puzzle quest")
    
    #Icône de la fenetre 
    pygame.display.set_icon(pygame.image.load('images/logo.png').convert_alpha())
    
    #Remplissage de la fenêtre en bleu
    fenetre.fill(blue)
    
    #Logo "chargement" 
    Logo_jeu = pygame.transform.scale(pygame.image.load("images/logo2.png").convert_alpha(),(650,650))
    fenetre.blit(Logo_jeu, (320,25))
    pygame.display.flip()
    
    #Délai de 3.5 sec avant affichage du Menu
    pygame.time.delay(delai)
    fenetre.fill(blue)
    fenetre.blit(pygame.transform.scale(Logo_jeu,(450,450)), (420,30))
    bouton_jouer = pygame.transform.scale(pygame.image.load("images/bouton.png").convert_alpha(),(500,150))
    fenetre.blit(bouton_jouer,(400,500))
    pygame.display.flip()
    
accueil(1000)

# BOUCLES PRINCIPALES
continuer_accueil = True #boucle d'accueil
continuer_partie = True #boucle pour faire plusieurs parties

#messages
pygame.font.init()
police = pygame.font.SysFont("GROBOLD.ttf", 45)

#Page d'accueil pour accéder à la grille 
while continuer_accueil:
    for event in pygame.event.get():        
        #Fermeture du jeu si il y a un evenement Quit
        if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
            print("Fin du jeu")
            pygame.quit()
            
        #Ouverture du jeu si clic sur le bouton jouer ou "ENTREE"
        if (event.type == MOUSEBUTTONDOWN and (415<event.pos[0]<881 and 515<event.pos[1]<634)) or (event.type == KEYDOWN and event.key == K_RETURN):
            continuer_accueil = False

while continuer_partie:
    #Joueurs
    joueur1= Joueur(50,0,0,1)
    joueur2= Joueur(50,0,0,1)
    liste_joueurs = [joueur1,joueur2]
    compteur = random.randint(0,1)

    # Objets principaux
    grille = Grille(8,8)
    afficheur = Afficheur(grille, 67)
    para = Parametre()
    grille.alea_grille()
    
    #réinitialisation des boucles
    continuer_choix = True #boucle choix mode de jeu
    continuer_jeu = True #boucle d'une partie
    continuer_fin = True #boucle de fin de partie

    while continuer_choix:
        fenetre.fill(blue)
        Logo_jeu = pygame.transform.scale(pygame.image.load("images/logo2.png").convert_alpha(),(450,450))
        fenetre.blit(Logo_jeu, (420,30))
        #Affichage du bouton du mode "1 joueur"
        bouton1 = pygame.transform.scale(pygame.image.load("images/bouton2.png").convert_alpha(),(375,113))
        fenetre.blit(bouton1,(250,525)) 
        #Affichage du bouton du mode "2 joueurs"
        bouton2 = pygame.transform.scale(pygame.image.load("images/bouton3.png").convert_alpha(),(375,113))
        fenetre.blit(bouton2,(675,525))                  
        pygame.display.flip()
        for event in pygame.event.get():
            if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                print("Fin du jeu")
                pygame.quit()

            if event.type == MOUSEBUTTONDOWN:
                y = pygame.mouse.get_pos()               
                #Bouton pour affichage du 1er mode 
                if 252<y[0]<617 and 527<y[1]<630: 
                    #mode 1 contre IA
                    mode = 1
                    continuer_choix = False
                #Bouton pour affichage du 2e mode
                if 678<y[0]<1043 and 533<y[1]<628:
                    #mdoe 1 contre 1
                    mode = 2
                    continuer_choix = False
                                    
    while continuer_jeu:
        joueur = liste_joueurs[compteur]
        continuer_joueur = True
        joueur.played = False #le joueur qui commence son tour n'a pas joué
        joueur.spe_def = False #le joueur qui commence son tour n'a plus son invulnérabilité
        boutique_ouverte = False #la boutique est fermée

        #1er affichage
        fenetre.blit(pygame.image.load("images/background.jpg").convert_alpha(), (0,0))
        afficher_elements(fenetre,joueur1,joueur2,joueur,police,mode,boutique_ouverte)
        afficheur.afficher(fenetre)
        pygame.display.flip()
        
        while continuer_joueur:
            while len(grille.liste_jouables())<1:
                grille.alea_grille() #changement de grille si plus de coup jouables

                #affichage au cas d'une reinitialisation de la grille
                fenetre.blit(pygame.image.load("images/background.jpg").convert_alpha(), (0,0))
                afficher_elements(fenetre,joueur1,joueur2,joueur,police,mode,boutique_ouverte)
                afficheur.afficher(fenetre)
                pygame.display.flip()

            #boutique de l'IA et passer tour de l'IA:
            if mode == 1 and joueur == joueur2:
                if joueur.argent >= 20 and joueur.vie <= 40:
                    achat(1000,560,joueur,joueur1,joueur2) #achat de vie
                elif joueur.argent >= 15 and joueur.vie >= 45:
                    achat(1000,600,joueur,joueur1,joueur2) #achat de bouclier

                while grille.meilleur_coup(grille.liste_jouables())[1] and joueur.jeton >= 2: #si le coup qui va etre joué est de type attaque
                    achat(1150,560,joueur,joueur1,joueur2) #achat de multiplicateur

                if not grille.meilleur_coup(grille.liste_jouables())[1] and joueur.jeton >= 10:
                    achat(1150,600,joueur,joueur1,joueur2) #achat d'invulnérabilité

                #affichage apres achat d'item
                fenetre.blit(pygame.image.load("images/background.jpg").convert_alpha(), (0,0))
                afficheur.afficher(fenetre)
                afficher_elements(fenetre,joueur1,joueur2,joueur,police,mode,boutique_ouverte)
                pygame.display.flip()

                if joueur.played:
                    continuer_joueur = False
                    
                    
            for event in pygame.event.get():
                click_grille = False
                
                if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                    print("Fin du jeu")
                    pygame.quit()
                
                if (event.type == MOUSEBUTTONDOWN) or (mode == 1 and joueur == joueur2): #si click ou si tour de l'IA

                    #CLICK PASSER TOUR
                    if (mode == 1 and joueur == joueur1) or mode == 2: #pas l'IA
                        if event.pos[0]>962 and event.pos[1]>403 and event.pos[0]<1291 and event.pos[1]<497 and joueur.played:
                            boutique_ouverte = False
                            continuer_joueur = False

                    #CLICKS BOUTIQUE
                    if (mode == 1 and joueur == joueur1) or mode == 2: #pas l'IA
                        if event.pos[0]>10 and event.pos[1]>400 and event.pos[0]<336 and event.pos[1]<495 and not boutique_ouverte:
                            boutique_ouverte = True #ouverture de boutique
                            afficher_elements(fenetre,joueur1,joueur2,joueur,police,mode,boutique_ouverte)
                            pygame.display.flip()

                        if (10<event.pos[0]<336 and 554<event.pos[1]<626) or (962<event.pos[0]<1290 and 554<event.pos[1]<626) and boutique_ouverte:
                            achat(event.pos[0],event.pos[1],joueur,joueur1,joueur2) #achat d'item

                            #affichage apres achat d'item
                            fenetre.blit(pygame.image.load("images/background.jpg").convert_alpha(), (0,0))
                            afficheur.afficher(fenetre)
                            afficher_elements(fenetre,joueur1,joueur2,joueur,police,mode,boutique_ouverte)
                            pygame.display.flip()

                    #CLICK GRILLE
                    if (mode == 1 and joueur == joueur1) or mode == 2: #pas l'IA, permet de 'navoir a verifier les clicks que s'il y en a, evite de planter
                        if (event.pos[0]>352 and event.pos[1]>94 and event.pos[0]<948 and event.pos[1]<690 and not joueur.played):
                            click_grille = True
                    if (mode == 1 and joueur == joueur2 and not joueur.played) or click_grille:
                        if mode == 1 and joueur == joueur2:
                            coup = grille.meilleur_coup(grille.liste_jouables())[0] #si tour de l'IA, choix du meilleur coup direct
                        else:
                            coup= action(event,para) #sinon, choix du coup via clicks
                        if coup != None:
                            dico_jouable = grille.liste_jouables()
                            if (coup[0],coup[1]) in dico_jouable.keys(): #si le coup est dans la liste des coups jouables                                   
                                grille.jouer_un_coup(dico_jouable,coup)
                                joueur.played = True

                                #affichage apres le changement de 2 cases
                                fenetre.blit(pygame.image.load("images/background.jpg").convert_alpha(), (0,0))
                                afficher_elements(fenetre,joueur1,joueur2,joueur,police,mode,boutique_ouverte)
                                afficheur.afficher(fenetre)
                                pygame.display.flip()
                                pygame.time.delay(500)

                                x = (grille.grille[dico_jouable[(coup[0],coup[1])][0][0]][dico_jouable[(coup[0],coup[1])][0][1]]) #x = contenu d'une des cases détruites
                                effet=Effet(x,len(dico_jouable[(coup[0],coup[1])])-1)
                                if joueur == joueur1:
                                    effet.action(joueur,joueur2)
                                else:
                                    effet.action(joueur,joueur1)
                                grille.casser(dico_jouable[(coup[0],coup[1])])                

                                #affichage apres le coup desiré
                                fenetre.blit(pygame.image.load("images/background.jpg").convert_alpha(), (0,0))
                                afficher_elements(fenetre,joueur1,joueur2,joueur,police,mode,boutique_ouverte)
                                afficheur.afficher(fenetre)
                                pygame.display.flip()
                                pygame.time.delay(500)
                                
                                while grille.etape_intermediaire_jouable(grille.grille)!=0: #combos avec les cases qui tombent
                                    x = grille.getGemme(grille.etape_intermediaire_jouable(grille.grille)[0][0],grille.etape_intermediaire_jouable(grille.grille)[0][1]) #x = contenu d'une des cases détruites
                                    effet = Effet(x,len(grille.etape_intermediaire_jouable(grille.grille))-1)
                                    if joueur == joueur1:
                                        effet.action(joueur,joueur2)
                                    else:
                                        effet.action(joueur,joueur1)
                                    grille.casser(grille.etape_intermediaire_jouable(grille.grille))

                                    #affichage de chaque combo
                                    fenetre.blit(pygame.image.load("images/background.jpg").convert_alpha(), (0,0))
                                    afficher_elements(fenetre,joueur1,joueur2,joueur,police,mode,boutique_ouverte)
                                    afficheur.afficher(fenetre)
                                    pygame.display.flip()
                                    pygame.time.delay(900)

                                joueur.spe_att = 1 #on reset le bonus d'attaque apres le coup joué

                                #VICTOIRE
                                if joueur1.vie == 0:
                                    fenetre.blit(pygame.transform.scale(pygame.image.load("images/findepartie2.png").convert_alpha(),(1000,434)), (160,130))
                                    pygame.display.flip()
                                    continuer_joueur = False
                                    continuer_jeu = False
                                if joueur2.vie == 0:
                                    fenetre.blit(pygame.transform.scale(pygame.image.load("images/findepartie.png").convert_alpha(),(1000,434)), (160,130))
                                    pygame.display.flip()
                                    continuer_joueur = False
                                    continuer_jeu = False

                                
        #changement de joueur                         
        compteur += 1
        if compteur == len(liste_joueurs):
            compteur = 0

    while continuer_fin:
        for event in pygame.event.get():
            if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE) or (event.type == MOUSEBUTTONDOWN and (709<event.pos[0]<1073 and 388<event.pos[1]<500)):
                print("Fin du jeu")
                pygame.quit()

            if event.type == MOUSEBUTTONDOWN and (214<event.pos[0]<577 and 388<event.pos[1]<503):
                continuer_fin = False
                


