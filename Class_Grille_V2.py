import random
import copy
from afficheur import *


class Grille:
    def __init__(self, longueur, largeur):
        self.grille = [0] * longueur
        for i in range(longueur):
            self.grille[i] = ['_'] * largeur
        self.longueur = longueur
        self.largeur = largeur

    def getGemme(self, x, y): #return la gemme de la case donnée
        return self.grille[x][y]

    def randomizer(self,x,y): #randomizer pour l'alea case
        randomizer = random.randint(1,100)
        if randomizer <= 25:
            self.grille[x][y]="att"
        elif 25 < randomizer <= 40:
            self.grille[x][y]="vie"
        elif 40 < randomizer <= 60:
            self.grille[x][y]="gre"
        elif 60 < randomizer <= 85:
            self.grille[x][y]="arg"
        elif randomizer > 85:
            self.grille[x][y]="jet"

    def alea_case(self,x,y): #création d'un jeton aléatoire
        self.randomizer(x,y)

        #on change la case au cas d'une combinaison pré-existante
        if x<2 and y>=2:
            while(self.grille[x][y-1]==self.grille[x][y] and self.grille[x][y-2]==self.grille[x][y]):
                self.randomizer(x,y)
                    
        elif y<2 and x>=2:
            while(self.grille[x-1][y]==self.grille[x][y] and self.grille[x-2][y]==self.grille[x][y]):
                self.randomizer(x,y)

        elif x>=2 and y>=2:
            while((self.grille[x-1][y]==self.grille[x][y] and self.grille[x-2][y]==self.grille[x][y]) or (self.grille[x][y-1]==self.grille[x][y] and self.grille[x][y-2]==self.grille[x][y])):
                self.randomizer(x,y)

        #2 prochains cas : lors de la creation de nouvelles cases apres destruction de jetons, au cas où un jeton est créé entre deux jetons identiques
        elif x>=1 and x<=(self.longueur-2):
            while (self.grille[x-1][y]==self.grille[x][y]) and (self.grille[x+1][y]==self.grille[x][y]):
                self.randomizer(x,y)

        elif y>=1 and y<=(self.largeur-2):
            while (self.grille[x][y-1]==self.grille[x][y]) and (self.grille[x][y+1]==self.grille[x][y]):
                self.randomizer(x,y)

            
    def alea_grille(self): #remplit la grille aléatoirement, est appelée si il n'y a pas de combinaison possible
        for x in range(self.longueur):
            for y in range(self.largeur):
                self.alea_case(x,y)


    def coup_jouable(self,grille,x,y): #fonction permettant de parcourir toute la grille afin de detecter un coup jouable(joué)
        i=x+1
        j=y+1 
        compteur = 1
        coup_jouable = [(x,y)] 
        while (i < self.longueur) and (grille[i][y] == grille[x][y]): #coup_jouable à la verticale
            compteur += 1
            coup_jouable.append((i,y))
            i+=1
        if compteur >= 3:
            coup_jouable.append("verti")
            return coup_jouable            

        i=x+1
        j=y+1 
        compteur = 1
        coup_jouable = [(x,y)]    
        while (j < self.largeur) and (grille[x][j] == grille[x][y]): #coup_jouable à l'horizontale                                    
            compteur += 1
            coup_jouable.append((x,j))
            j+=1
        if compteur >= 3:
            coup_jouable.append("hori")
            return coup_jouable
        
        return 0

    def etape_intermediaire_jouable(self,grille): #fait la liste des cases détruites pour chaque coup avec la foction précédente
        gemmes_detruites = []
        for x in range(self.longueur):
            for y in range(self.largeur):
                if self.coup_jouable(grille,x,y) != 0:
                    gemmes_detruites.append(self.coup_jouable(grille,x,y))
        if gemmes_detruites != []:
            return gemmes_detruites[0]
        else:
            return 0
                    
    def liste_jouables(self): #Fonction qui simule tous les choix que le joueur peut faire et retourne la liste des coups jouables
        directions = [(0,1),(1,0),(0,-1),(-1,0)]
        liste_jouables={}
        for x in range(self.longueur):
            for y in range(self.largeur):
                for d in directions:
                    (X,Y)=(x+d[0],y+d[1])
                    g_test = copy.deepcopy(self.grille) #la fonction deepcopy permet de creer une grille equivalente a self.grille que l'on peut modifier sans modifier self.grille
                    if (X>=0) and (Y>=0) and (X<self.longueur) and (Y<self.largeur) and (self.grille[x][y] != self.grille[X][Y]):
                        g_test[X][Y] = self.grille[x][y]
                        g_test[x][y] = self.grille[X][Y]
                        if self.etape_intermediaire_jouable(g_test) != 0:
                            liste_jouables[(x,y),(X,Y)]=self.etape_intermediaire_jouable(g_test)
                                                                                                                                                                                      
        return liste_jouables
        #liste_jouables est un dictionnaire de tous les tuples correspondant aux coups jouables : 
        #(<coordonnées des cases interverties> : <liste des coordonnées des cases détruites (prend donc en compte la case bougée de base)>)

    def casser(self,jetons): #fonction qui détruit ue liste de jetons, change la grille en conséquence, et refait de nouveaux jetons
        if jetons[-1]=="hori":
            liste_y=[]
            for (x,y) in jetons[:-1]:
                if x>0:
                    del(self.grille[x][y])
                    while x>0:
                        if x==1:
                            self.grille[x].insert(y, self.grille[x-1][jetons[0][1]])
                            del(self.grille[x-1][jetons[0][1]])
                            self.grille[0].insert(y, '_')
                        else:
                            self.grille[x].insert(y, self.grille[x-1][y])
                            del(self.grille[x-1][y])
                        x-=1
                else:
                    del(self.grille[x][jetons[0][1]])
                    self.grille[0].insert(y, '_')
                    
        elif jetons[-1]=="verti":
            jetons_bis = jetons[:-1]
            for (x,y) in jetons_bis[::-1]:
                del(self.grille[x][y])
                if x-len(jetons_bis) >= 0:
                    self.grille[x].insert(y, self.grille[x-len(jetons_bis)][y])
                    del(self.grille[x-len(jetons_bis)][y])
                    self.grille[x-len(jetons_bis)].insert(y, '_')
                else:
                    self.grille[x].insert(y, '_')
                    self.alea_case(x,y)
                    
        for a in range(self.longueur):
            for b in range(self.largeur):
                if self.grille[a][b] == '_':
                    self.alea_case(a,b)
        
    def jouer_un_coup(self,liste_jouables,coords_jouees): #fonction qui intervertit les 2 cases séléctionnées par le joueur
        liste_coords = []
        jetons_a_casser = []
        for cle in liste_jouables.keys():
            liste_coords.append(cle)
        ((x1,y1),(x2,y2)) = (coords_jouees[0],coords_jouees[1])
        if ((x1,y1),(x2,y2)) in liste_coords:
            jetons_a_casser = liste_jouables[((x1,y1),(x2,y2))]
            a = self.grille[x2][y2]
            self.grille[x2][y2]=self.grille[x1][y1]
            self.grille[x1][y1]=a

    def meilleur_coup(self, liste_jouables): #fonction pour l'IA, permettant de séléctionner le                                             
        meilleur_coup = []                   #meilleur coup (sans prendre en compte les retombées de cases)
        meilleur_coup_returned = 0
        for a in liste_jouables:
            if ((len(liste_jouables[a]) > len(meilleur_coup) and self.grille[liste_jouables[a][0][0]][liste_jouables[a][0][1]] != "gre") or
               (len(liste_jouables[a]) == len(meilleur_coup) and self.grille[liste_jouables[a][0][0]][liste_jouables[a][0][1]] != "att")):
                meilleur_coup = liste_jouables[a]
                meilleur_coup_returned = a        
        if self.grille[liste_jouables[a][0][0]][liste_jouables[a][0][1]] == "att":
            return [[a[0],a[1]], True] #True = cases attaques qui vont etre détruites, si c'est le cas, l'IA achetera, si il peut, un bonus d'attaque
        else:
            return [[a[0],a[1]], False]
