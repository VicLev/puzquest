import pygame
from Class_Grille_V2 import *
from pygame.locals import *

COLOR = {"att":pygame.transform.scale(pygame.image.load("images/attaque.png"),(67,67)),
         "vie":pygame.transform.scale(pygame.image.load("images/vie.png"),(67,67)),
         "gre":pygame.transform.scale(pygame.image.load("images/gre.png"),(67,67)),
         "jet":pygame.transform.scale(pygame.image.load("images/jeton.png"),(67,67)),
         "arg":pygame.transform.scale(pygame.image.load("images/argent.png"),(67,67))
         }

#67px les cases, 8px les entre-deux

class Afficheur:
        def __init__(self, grille, size):
                self.magrille = grille
                self.size = size
                
        def afficher(self,fenetre):
                for x in range(self.magrille.longueur):
                        for y in range(self.magrille.largeur):
                                gemme = self.magrille.getGemme(y,x)
                                if gemme in COLOR:
                                        fenetre.blit(COLOR[gemme],(352+(x*self.size)+(x*8.5),94+(y*self.size)+(y*8.5)))

                        
class Parametre:
        def __init__(self):
                self.click1= None
                self.click2=None
                self.liste_click=[]

def action(event,parametre):
        parametre.liste_click[:]=[]
        if parametre.click1 == None and event.pos[0]>352 and event.pos[1]>94 and event.pos[0]<948 and event.pos[1]<690:
                parametre.click1=(int((event.pos[1]-94)/75),int((event.pos[0]-352)/75))
        else:
                if  event.pos[0]>352 and event.pos[1]>94 and event.pos[0]<948 and event.pos[1]<690:
                        parametre.click2=(int((event.pos[1]-94)/75),int((event.pos[0]-352)/75))
                        parametre.liste_click.append(parametre.click1)
                        parametre.liste_click.append(parametre.click2)
        if parametre.click1 != None and parametre.click2 !=None:
                parametre.click1,parametre.click2 = None,None
                return parametre.liste_click

def afficher_elements(fenetre,j1,j2,j,police,mode,boutique_ouverte): #affichage de tous les textes, icone de coups spéciaux, etc ..
        constante = police.render("/ 50",1,(238,238,238))
        fenetre.blit(constante, (226, 347))
        fenetre.blit(constante, (1179, 347))
        fenetre.blit(constante, (226, 284))
        fenetre.blit(constante, (1179, 284))
        
        argentj1 = police.render(str(j1.argent),1,(238,238,238))
        fenetre.blit(argentj1, (180, 134))
        jetonj1 = police.render(str(j1.jeton),1,(238,238,238))
        fenetre.blit(jetonj1, (180, 210))
        bouclierj1 = police.render(str(j1.bouclier),1,(238,238,238))
        fenetre.blit(bouclierj1, (180, 284))
        viej1 = police.render(str(j1.vie),1,(238,238,238))
        fenetre.blit(viej1, (180, 347))
        
        argentj2 = police.render(str(j2.argent),1,(238,238,238))
        fenetre.blit(argentj2, (1132, 130))
        jetonj2 = police.render(str(j2.jeton),1,(238,238,238))
        fenetre.blit(jetonj2, (1132, 210))
        bouclierj2 = police.render(str(j2.bouclier),1,(238,238,238))
        fenetre.blit(bouclierj2, (1132, 284))
        viej2 = police.render(str(j2.vie),1,(238,238,238))
        fenetre.blit(viej2, (1132, 347))

        message_j1_1 = police.render("J1 : A toi de jouer !",1,(238,238,238))
        message_j1_2 = police.render("J1 : Attends ton tour",1,(238,238,238))
        message_j2_1 = police.render("J2 : A toi de jouer !",1,(238,238,238))
        message_j2_2 = police.render("J2 : Attends ton tour",1,(238,238,238))
        message_ia_1 = police.render("IA : A toi de jouer !",1,(238,238,238))
        message_ia_2 = police.render("IA : Attends ton tour",1,(238,238,238))

        boutique = pygame.transform.scale(pygame.image.load("images/boutique.png").convert_alpha(),(335,80))
        if mode == 1:
                if j == j1:
                        fenetre.blit(message_j1_1, (47, 23))
                        fenetre.blit(message_ia_2, (950, 23))
                        if boutique_ouverte:
                                fenetre.blit(boutique, (5,550))
                else:
                        fenetre.blit(message_j1_2, (47, 23))
                        fenetre.blit(message_ia_1, (950, 23))
        if mode == 2:
                if j == j1:
                        fenetre.blit(message_j1_1, (47, 23))
                        fenetre.blit(message_j2_2, (950, 23))
                        if boutique_ouverte:
                                fenetre.blit(boutique, (5,550))
                else:
                        fenetre.blit(message_j1_2, (47, 23))
                        fenetre.blit(message_j2_1, (950, 23))
                        if boutique_ouverte:
                                fenetre.blit(boutique, (957,550))

        fenetre.blit(pygame.transform.scale(pygame.image.load("images/attaque.png").convert_alpha(),(30,30)),(202,72))
        fenetre.blit(pygame.transform.scale(pygame.image.load("images/attaque.png").convert_alpha(),(30,30)),(1151,72))
        x = police.render("x",1,(54,60,170))
        fenetre.blit(x, (235, 78))
        fenetre.blit(x, (1184, 78))
        j1spe_att = police.render(str(j1.spe_att),1,(54,60,170))
        j2spe_att = police.render(str(j2.spe_att),1,(54,60,170))
        fenetre.blit(j1spe_att, (260, 78))
        fenetre.blit(j2spe_att, (1209, 78))
        if j1.spe_def:
                fenetre.blit(pygame.transform.scale(pygame.image.load("images/effet1.png").convert_alpha(),(90,30)),(56,72))
        if j2.spe_def:
                fenetre.blit(pygame.transform.scale(pygame.image.load("images/effet1.png").convert_alpha(),(90,30)),(1005,72))
