# Puzzle Quest

Le Puzzle Quest est un jeu de puzzle type "Candy Crush" qui se joue en 1 vs 1 et au tour par tour. Il est possible de jouer à 2 joueurs ou 1 joueur contre une IA.  
L'objectif est de réduire le nombre de coeurs de l'adversaire à 0.  
Il est possible d'interchanger de position 2 cases, permettant de réaliser des combos (des successions en ligne ou colonne d'un meme type de case, exemple : 4 pièces d'affilé)  

Chaque type de case a un effet particulier :  
- Les coeurs : permettent de récupérer le nombre de coeur du combo
- Les épées : permettent de réduire le nombre de coeur adverse d'un montant égal au combo * le multiplicateur (en haut à gauche)
- Les jetons : permettent des achats dans la boutique, du multiplicateur d'attaque pour la prochaine attaque ou une parade complete de la prochaine attaque adverse
- Les pièces : permettent des achats dans la boutique, des coeurs ou du bouclier, receptionnant les dégats avant les coeurs
- Les tetes de mort : font subir des dégats au joueur en train de jouer

La condition minimale pour pouvoir passer son tour est d'avoir interchangé 2 cases formant un combo, autrement, toutes les actions de boutique sont possibles pendant un tour, permettant de grandes attaques via la boutique.  

Pour jouer :  
`python main.py` (nécessite pygame et python 3+)
