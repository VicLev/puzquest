class Joueur():
    def __init__(self, vie, bouclier, argent, jeton):
            self.vie = vie
            self.bouclier = bouclier
            self.argent = argent
            self.jeton = jeton
            self.spe_att = 1
            self.spe_def = False
            self.played = False

class Effet:
    def __init__(self,couleur,nombre):
        self.couleur = couleur
        self.nombre = nombre

    def action(self, j_actif, j_inactif): #effet différent pour chaque couleur (= case)
        if self.couleur == 'att':
            degats = self.nombre * j_actif.spe_att
            if j_inactif.spe_def == True:
                pass
            else:
                while j_inactif.bouclier > 0 and degats > 0: 
                    j_inactif.bouclier -= 1
                    degats -= 1
                while j_inactif.vie > 0 and degats > 0:
                    j_inactif.vie -= 1
                    degats -= 1

        if self.couleur == 'vie':
            vie = self.nombre
            while j_actif.vie < 50 and vie > 0:
                j_actif.vie += 1
                vie -= 1
                
        if self.couleur == 'arg':
            j_actif.argent += self.nombre * 3
            
        if self.couleur == 'gre':
            if j_actif.spe_def == True:
                degats = 0
            else:
                degats = self.nombre
            while j_actif.bouclier > 0 and degats > 0: 
                j_actif.bouclier -= 1
                degats -= 1
            while j_actif.vie > 0 and degats > 0:
                j_actif.vie -= 1
                degats -= 1

        if self.couleur =='jet':
            j_actif.jeton += self.nombre

def achat(x,y,joueur,j1,j2):
    if joueur == j1:
        if 10<x<170 and 553<y<587: #achat de 10pv pour 20argent
            if joueur.argent >= 20 and joueur.vie <50:
                vie = 10
                joueur.argent -= 20
                while joueur.vie < 50 and vie > 0:
                    joueur.vie += 1
                    vie -= 1
                    
        elif 10<x<170 and 595<y<627: #achat de 5shield pour 15argent
            if joueur.argent >= 15 and joueur.bouclier < 50:
                bouclier = 5
                joueur.argent -= 15
                while joueur.bouclier < 50 and bouclier > 0:
                    joueur.bouclier += 1
                    bouclier -= 1

        elif 177<x<337 and 554<y<585: #achat de multiplicateur pour 2jetons
            if joueur.jeton >= 2:
                joueur.jeton -= 2
                joueur.spe_att = joueur.spe_att * 2

        elif 177<x<337 and 595<y<626: #achat de super shield pour 5jetons
            if joueur.jeton >= 5 and not joueur.spe_def:
                joueur.jeton -= 5
                joueur.spe_def = True
        else:
            pass
    
    if joueur == j2:
        if 960<x<1122 and 553<y<587: #achat de 10pv pour 20argent
            if joueur.argent >= 20 and joueur.vie <50:
                vie = 10
                joueur.argent -= 20
                while joueur.vie < 50 and vie > 0:
                    joueur.vie += 1
                    vie -= 1                    
        elif 960<x<1122 and 595<y<627: #achat de 5shield pour 15argent
            if joueur.argent >= 15 and joueur.bouclier < 50:
                bouclier = 5
                joueur.argent -= 15
                while joueur.bouclier < 50 and bouclier > 0:
                    joueur.bouclier += 1
                    bouclier -= 1

        elif 1129<x<1288 and 554<y<585: #achat de multiplicateur pour 2jetons
            if joueur.jeton >= 2:
                joueur.jeton -= 2
                joueur.spe_att = joueur.spe_att * 2

        elif 1129<x<1288 and 595<y<626: #achat de super shield pour 5jetons
            if joueur.jeton >= 5 and not joueur.spe_def:
                joueur.jeton -= 5
                joueur.spe_def = True
        else:
            pass
    
    


        














        
